const PipelineDataSource = require("./PipelineDataSource");
const defaults = require("lodash.defaultsdeep");

class PipelineScraperGUI extends PipelineDataSource {

  constructor(opts) {
    super(defaults(opts, {
    }));
  }

  check() {
    this.active.set(false);
  }

  poll() {
    this.pipeline.set({});
  }

}

module.exports = PipelineScraperGUI;
