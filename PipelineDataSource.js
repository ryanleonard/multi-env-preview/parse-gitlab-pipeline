const { atom, struct } = require("derivable");
const defaults = require("lodash.defaultsdeep");

/**
 * Abstract source of GitLab pipeline data.
*/
class PipelineDataSource {

  /**
   * Utility to poll a callback based on {@link Atom} objects.
   * Uses `setInterval` under the hood.
   * @param {Atom<Boolean>} enabled will poll `cb` if `true`.
   * @param {Atom<Number>} interval the frequency to poll `cb`.
   * @param {Function} cb the function to call each `interval`.
  */
  static poll(enabled, interval, cb) {
    let poll;
    struct({ enabled, interval })
      .react(opts => {
        clearInterval(poll);
        if(opts.enabled) {
          poll = setInterval(cb, opts.interval);
        }
      });
  }

  constructor(opts) {
    opts = defaults(opts, {
      enabled: atom(true),
      enableChecking: atom(true),
      checkInterval: atom(1000),
      pollInterval: atom(2000),
    });
    this.enabled = opts.enabled;
    this.enableChecking = opts.enableChecking;
    this.checkInterval = opts.checkInterval;
    this.pollInterval = opts.pollInterval;

    this.pipeline = atom();

    this.constructor.poll(
      this.enabled.and(this.enableChecking),
      this.checkInterval, () => this.check()
    );
    this.active = atom(false);

    this.valid = this.enabled.and(this.active);

    this.constructor.poll(this.valid, this.pollInterval, () => this.poll());
  }

  /**
   * Check to ensure that this data source can still be used.
   * Updates `this.active`.
  */
  check() {
    this.active.set(true);
  }

  /**
   * Update the data this source exports.
   * Updates `this.pipeline`.
  */
  poll() {
    this.pipeline.set({});
  }

  /**
   * Get a specific stage and job from the pipeline this data source parsed.
   * @param {String} stage the name of the pipeline stage to query.
   * @param {String} job the name of the pipeline job to query.
   * @return {Derivable<null|PipelineJobData>} the data associated with
   *   this pipeline job.
  */
  job(stage, job) {
    return this.pipeline.mDerive(stage).mDerive(job);
  }

}

module.exports = PipelineDataSource;
