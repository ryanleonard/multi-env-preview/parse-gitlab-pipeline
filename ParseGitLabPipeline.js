const {
  atom,
  struct,
  isAtom,
  isDerivable,
} = require("derivable");
const PipelineScraperGUI = require("./PipelineScraperGUI");
const PipelineScraperAPI = require("./PipelineScraperAPI");

class ParseGitLabPipeline {

  static get isBrowser() {
    return typeof window === "object";
  }

  static get globalName() {
    const prefix = "multi-env-preview";
    return {
      class: `${prefix}.ParseGitLabPipeline`,
      instance: `${prefix}.parse-gitlab-pipeline`,
    };
  }

  constructor() {
    this.inBrowser = atom(this.constructor.isBrowser);
    this.sources = [];

    this.gui = new PipelineScraperGUI({
      enabled: this.inBrowser,
      checkInterval: atom(200),
      pollInterval: atom(200),
    });

    this.api = new PipelineScraperAPI({
      enabled: this.inBrowser.and(this.gui.valid.not()),
      checkInterval: atom(200),
      pollInterval: atom(2000),
    });

    this.sources.push(this.gui);
    this.sources.push(this.api);
  }

  /**
   * Get a specific stage and job from the parsed pipeline.
   * @param {String} stage the name of the pipeline stage to query.
   * @param {String} job the name of the pipeline job to query.
   * @return {Derivable<null|PipelineJobData>} the data associated with
   *   this pipeline job.
  */
  job(stage, job) {
    let out;
    for(const source of this.sources) {
      const value = source.valid.and(source.job(stage, job));
      if(!isAtom(out) && !isDerivable(out)) {
        out = value;
      }
      else {
        out = out.or(value);
      }
    }
    return out;
  }

}

module.exports = ParseGitLabPipeline;
