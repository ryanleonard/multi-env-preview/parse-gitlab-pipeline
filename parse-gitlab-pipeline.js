const ParseGitLabPipeline = require("./ParseGitLabPipeline");

if(ParseGitLabPipeline.isBrowser) {
  const name = ParseGitLabPipeline.globalName;
  if(typeof window[name.class] !== "function") {
    window[name.class] = ParseGitLabPipeline;
  }
  if(typeof window[name.instance] !== "object") {
    window[name.instance] = new ParseGitLabPipeline();
    module.exports = window[name.instance];
  }
}
else {
  module.exports = new ParseGitLabPipeline();
}
