const test = require("ava");
const ParseGitLabPipeline = require("ParseGitLabPipeline");

test("isBrowser is false", t => {
  t.plan(1);
  t.is(ParseGitLabPipeline.isBrowser, false);
});
