const test = require("ava");
const ParseGitLabPipeline = require("ParseGitLabPipeline");

const STAGE_NAME = "test";
const JOB_NAME = "npm test";
const GUI_DATA = 5;
const API_DATA = 10;

test.beforeEach("create ParseGitLabPipeline", t => {
  const pipeline = new ParseGitLabPipeline();
  pipeline.gui.enableChecking.set(false);
  pipeline.api.enableChecking.set(false);
  pipeline.gui.pipeline.set({
    [STAGE_NAME]: {
      [JOB_NAME]: GUI_DATA,
    },
  });
  pipeline.api.pipeline.set({
    [STAGE_NAME]: {
      [JOB_NAME]: API_DATA,
    },
  });
  t.context.pipeline = pipeline;
});

test("returns 'false' if both disabled", t => {
  t.plan(1);
  const { pipeline } = t.context;
  pipeline.inBrowser.set(false);
  t.is(pipeline.job(STAGE_NAME, JOB_NAME).get(), false);
});

test("returns value from 'gui' by default", t => {
  t.plan(1);
  const { pipeline } = t.context;
  pipeline.gui.enabled.set(true);
  pipeline.gui.active.set(true);
  t.is(pipeline.job(STAGE_NAME, JOB_NAME).get(), GUI_DATA);
});

test("returns value from 'api' if gui disabled and api enabled", t => {
  t.plan(1);
  const { pipeline } = t.context;
  pipeline.gui.enabled.set(false);
  pipeline.gui.active.set(false);
  pipeline.api.enabled.set(true);
  pipeline.api.active.set(true);
  t.is(pipeline.job(STAGE_NAME, JOB_NAME).get(), API_DATA);
});
