const test = require("ava");
const { atom } = require("derivable");
const PipelineDataSource = require("PipelineDataSource");

const STAGE_NAME = "test";
const JOB_NAME = "npm test";
const JOB_DATA = 5;

const FULL_DATA = {
  [STAGE_NAME]: {
    [JOB_NAME]: JOB_DATA,
  },
};

const EMPTY_STAGE = {
  [STAGE_NAME]: {},
};

test.beforeEach("create PipelineDataSource", t => {
  t.context.pipeline = new PipelineDataSource({
    enabled: atom(true),
    enableChecking: atom(false),
  });
});

test("returns data from pipeline stages", t => {
  const { pipeline } = t.context;
  pipeline.pipeline.set(FULL_DATA);
  t.is(pipeline.job(STAGE_NAME, JOB_NAME).get(), JOB_DATA);
});

test("returns 'undefined' if stage is empty", t => {
  const { pipeline } = t.context;
  pipeline.pipeline.set(EMPTY_STAGE);
  t.is(pipeline.job(STAGE_NAME, JOB_NAME).get(), undefined);
});

test("returns 'undefined' if there isn't any stage data", t => {
  const { pipeline } = t.context;
  pipeline.pipeline.set({});
  t.is(pipeline.job(STAGE_NAME, JOB_NAME).get(), undefined);
});
