const path = require("path");
const test = require("ava");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const ParseGitLabPipeline = require("ParseGitLabPipeline");

const loadScript = path.resolve(__dirname, "helpers/load-script.html");

function delay(time) {
  return new Promise(resolve => setTimeout(resolve, time));
}

test(`sets '${ParseGitLabPipeline.globalName.class}'`, async function(t) {
  t.plan(1);
  const dom = await JSDOM.fromFile(loadScript, {
    runScripts: "dangerously",
    resources: "usable",
  });
  await delay(100);
  t.is(typeof dom.window[ParseGitLabPipeline.globalName.class], "function");
  dom.window.close();
});

test(`sets '${ParseGitLabPipeline.globalName.instance}'`, async function(t) {
  t.plan(1);
  const dom = await JSDOM.fromFile(loadScript, {
    runScripts: "dangerously",
    resources: "usable",
  });
  await delay(100);
  t.is(typeof dom.window[ParseGitLabPipeline.globalName.instance], "object");
  dom.window.close();
});

test(`doesn't change existing '${ParseGitLabPipeline.globalName.instance}'`, async function(t) {
  t.plan(1);
  const dom = await JSDOM.fromFile(loadScript, {
    runScripts: "dangerously",
    resources: "usable",
    beforeParse(window) {
      window[ParseGitLabPipeline.globalName.instance] = { foo: 5 };
    },
  });
  await delay(100);
  t.is(dom.window[ParseGitLabPipeline.globalName.instance].foo, 5);
  dom.window.close();
});
